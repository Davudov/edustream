package aze.app.edustream.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import aze.app.edustream.R
import aze.app.edustream.student.StudentActivity
import aze.app.edustream.teacher.TeacherActivity
import aze.app.edustream.util.FireStoreUtil
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast


class LoginActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    private val RC_SIGN_IN = 1
    private var type = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        auth = FirebaseAuth.getInstance()
        type =
            getSharedPreferences("user_preferences", Context.MODE_PRIVATE).getString("type", "")!!
        google_sign_in_btn.setOnClickListener {
            signIn()
        }

    }

    override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        if (currentUser != null)
            updateUI(currentUser)
    }

    private fun updateUI(currentUser: FirebaseUser?) {
        toast("Logged in as ${currentUser?.email}")
//        user_name.text = currentUser?.email
    }


    private fun signIn() {
      FireStoreUtil.getClient(this){
          startActivityForResult(it.signInIntent, RC_SIGN_IN)
      }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sig   n In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)!!
                Log.d("$this", "firebaseAuthWithGoogle:" + account.id)
                firebaseAuthWithGoogle(account.idToken!!)
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.w("$this", "Google sign in failed", e)
                // ...
            }
        }
    }

    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("$this", "signInWithCredential:success")
                    finishActivity()
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("$this", "signInWithCredential:failure", task.exception)
                    // ...
                    activity_login_parent.snackbar("Authentication Failed.")
                    updateUI(null)
                }

                // ...
            }
    }

    private fun finishActivity() {
        FireStoreUtil.getOrCreateCurrentUser(type) { uid, userType, exists ->
            if (exists && type != userType) {
                alert {
                    isCancelable = false
                    title = "Alert!!"
                    message =
                        "Your user type is $userType do you really want to change it to $type?"
                    positiveButton("YES") {
                        FireStoreUtil.changeUserType(type) {
                            finishWith(type, uid)
                        }
                    }
                    negativeButton("NO") {
                        finishWith(userType, uid)
                    }
                }.show()
                return@getOrCreateCurrentUser
            }
            finishWith(userType, uid)
        }
    }

    private fun finishWith(userType: String, uid: String) {
        if (userType == "Student") {
            startActivity<StudentActivity>("uid" to uid)
        } else if (userType == "Teacher") {
            startActivity<TeacherActivity>("uid" to uid)
        }
        finish()
    }
}

