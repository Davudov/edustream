package aze.app.edustream.teacher

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import aze.app.edustream.R
import aze.app.edustream.util.FireStoreUtil
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_teacher.*


class TeacherActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_teacher)
        log_out_btn.setOnClickListener {
            FireStoreUtil.getClient(this) {
                it.signOut().addOnCompleteListener(
                    this
                ) {
                    FirebaseAuth.getInstance().signOut()
                    finish()
                }

            }
        }


    }
}