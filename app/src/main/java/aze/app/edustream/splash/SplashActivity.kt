package aze.app.edustream.splash

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import aze.app.edustream.R
import aze.app.edustream.select_type.SelectTypeActivity
import aze.app.edustream.student.StudentActivity
import aze.app.edustream.teacher.TeacherActivity
import aze.app.edustream.util.FireStoreUtil
import com.google.firebase.auth.FirebaseAuth
import org.jetbrains.anko.startActivity

class SplashActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    private lateinit var runnable: Runnable

    var mHandler: Handler = Handler()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        auth = FirebaseAuth.getInstance()

        runnable = Runnable {
            if (auth.currentUser != null) {
                FireStoreUtil.getOrCreateCurrentUser { _, userType, _ ->
                    if (userType == "Student")
                        startActivity<StudentActivity>()
                    else if (userType == "Teacher")
                        startActivity<TeacherActivity>()
                    finish()
                }
            } else {
                startActivity<SelectTypeActivity>()
                finish()
            }
        }
        mHandler.postDelayed(runnable, 1000)
    }

    override fun onStop() {
        super.onStop()
        mHandler.removeCallbacks(runnable)
    }
}