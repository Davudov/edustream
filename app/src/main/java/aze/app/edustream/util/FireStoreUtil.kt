package aze.app.edustream.util

import android.app.Activity
import aze.app.edustream.R
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import entity.User

object FireStoreUtil {
    private val firestoreInstance: FirebaseFirestore by lazy { FirebaseFirestore.getInstance() }

    private val currentUserDocRef: DocumentReference
        get() = firestoreInstance.document(
            "users/${FirebaseAuth.getInstance().currentUser?.uid
                ?: throw NullPointerException("UID is null.")}"
        )


    fun changeUserType(type: String, onComplete: () -> Unit) {
        currentUserDocRef.set(
            mapOf(
                "uid" to FirebaseAuth.getInstance().currentUser!!.uid,
                "email" to FirebaseAuth.getInstance().currentUser!!.email,
                "type" to type
            )
        ).addOnSuccessListener {
            onComplete()

        }
    }

    fun getOrCreateCurrentUser(
        type: String = "",
        onComplete: (uid: String, type: String, exists: Boolean) -> Unit
    ) {
        currentUserDocRef.get()
            .addOnSuccessListener {
                if (it.exists()) {
                    onComplete(
                        it.toObject(User::class.java)?.uid!!,
                        it.toObject(User::class.java)?.type!!,
                        true
                    )
                } else {
                    val currentUser = FirebaseAuth.getInstance().currentUser!!
                    currentUserDocRef.set(
                        mapOf(
                            "uid" to currentUser.uid,
                            "email" to currentUser.email,
                            "type" to type
                        )
                    )
                    onComplete(currentUser.uid, type, false)
                }
            }
    }

    fun getClient(
        activity: Activity,
        onComplete: (googleSignInClient: GoogleSignInClient) -> Unit
    ) {
        val gso =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(activity.getString(R.string.default_web_client_id))
                .requestEmail()
                .build()
        onComplete(GoogleSignIn.getClient(activity, gso))
    }

}