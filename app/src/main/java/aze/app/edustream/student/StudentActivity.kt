package aze.app.edustream.student

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import aze.app.edustream.R
import aze.app.edustream.util.FireStoreUtil
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_student.*


class StudentActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_student)
        log_out_btn.setOnClickListener{
            FireStoreUtil.getClient(this) {
                it.signOut().addOnCompleteListener(
                    this
                ) {
                    FirebaseAuth.getInstance().signOut()
                    finish()
                }
            }
        }
    }
}