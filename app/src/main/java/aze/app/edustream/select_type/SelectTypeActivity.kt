package aze.app.edustream.select_type

import android.content.Context
import android.os.Bundle
import android.widget.RadioButton
import androidx.appcompat.app.AppCompatActivity
import aze.app.edustream.R
import aze.app.edustream.login.LoginActivity
import kotlinx.android.synthetic.main.select_type.*
import org.jetbrains.anko.startActivity

class   SelectTypeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.select_type)
        val sharedPreference = getSharedPreferences("user_preferences", Context.MODE_PRIVATE)
        val editor = sharedPreference.edit()

        save_btn.setOnClickListener {
            editor.putString(
                "type",
                radioGroup.findViewById<RadioButton>(radioGroup.checkedRadioButtonId).tag as String
            )
            editor.apply()
            startActivity<LoginActivity>()
            finish()
        }

    }
}